import tornado.web
from sqlalchemy.orm import (
    sessionmaker,
    Load
)
from sqlalchemy import create_engine
from contextlib import contextmanager
import sqlalchemy as sa
from models import (
    Sense,
    Term,
    SenseTerm,
    Variant,
    VariantUsage
)
import math

class DBHandler():
    def __init__(self, url):
        self.engine = create_engine(url)
        self.sessionmaker = sessionmaker(bind=self.engine)

    @contextmanager
    def session_manager(self):
        self.session = self.sessionmaker()
        try:
            yield
            self.session.commit()
        except:
            self.session.rollback()
            raise
        finally:
            self.session.close()

    ## Merkitys ##

    def get_sense_id(self, identifier):
        # Haetaan yksi merkitys
        sense = self.session.query(Sense).filter(Sense.identifier == identifier).one_or_none()
        if not sense:
            raise tornado.web.HTTPError(404)
        return sense

    def get_sense_family(self, identifier, height, depth, width, parent_page, child_page):
        # Haetaan merkitys ja 
          # height verran sen vanhempia
          # depth syvyydeltä lapsia
          # width verran lapsia per syvyyden taso
        main_sense = self.get_sense_id(identifier)
        parents = []
        children = []
        counter = 0
        parent = main_sense.parent
        while counter < height and parent is not None:
            parents.append(dict(identifier=parent.identifier, sense_text=parent.sense, description=parent.description))
            parent = parent.parent
            counter += 1
        # Käännetään vanhempien järjestys, että vanhin on esimmäisenä 
        parents = reversed(parents)
        # Sivujen määrä nykyisellä sivun koolla
        parent_page_total = math.ceil(counter / height)
        sense_page_total = math.ceil(len(main_sense.children) / width)
        # Haetaan merkityksen lapset
        children = self.get_sense_child_list(main_sense, depth, width, child_page)
        return dict(parents=parents, sense=dict(identifier=main_sense.identifier, sense_text=str(main_sense.sense),
                                                description=str(main_sense.description)),
                    children=children['children'], parent_page_total=parent_page_total, sense_page_total=sense_page_total)

    def get_sense_child_list(self, parent, depth, width, page):
        # syötteenä lapsi elementti, palauttaa listan lapsistaan ja niille listat niiden lapsista määriteltyyn syvyyteen
        depth -= 1
        child_list = []
        if depth >= 0:
            # Järjestetään lista vanhemman lapsista, ja haetaan halutulle sivunumerolle kuuluvat lapset
            children = sorted(parent.children, key=lambda x: x.sense)[width*page:width*(page+1)]
            for child in children:
                # haetaan vain tieto onko lapsella lapsia
                child_list.append(self.get_sense_child_list(child, depth, 1, 0))
        return dict(identifier=parent.identifier, sense_text=str(parent.sense), description=str(parent.description), children=child_list)

    def get_all_sense_children(self, parent):
        # Haetaan merkityksen kaikki lapset
        child_list = []
        for child in parent.children:
            child_list.append(self.get_all_sense_children(child))
        return dict(identifier=parent.identifier, sense_text=str(parent.sense), description=str(parent.description), children=child_list)

    def get_sense_thesaurus(self):
        # Noudetaan koko merkitystesaurus 
        # Etsitään ensin kaikki juurimerkityksien tietokantatunnisteet
        roots = self.session.query(Sense).filter(Sense.parent_id == None).all()
        root_list = []
        for root in roots:
            children = self.get_all_sense_children(root)
            root_list.append(dict(sense=dict(identifier=root.identifier, sense_text=str(root.sense),
                                             description=str(root.description)), children=children['children']))
        return root_list

    def get_sense_terms(self, identifier, height, page):
        # Haetaan merkitykseen liittyvät termit
        terms = []
        sense = self.get_sense_id(identifier)
        term_page_total = math.ceil(len(sense.sense_terms)/height)
        single_term_page = sense.sense_terms[height*page:height*(page+1)]
        for sense_term in single_term_page:
            terms.append(dict(identifier=sense_term.term_id,
                              term_text=str(self.get_term_id(sense_term.term_id).term)))
        return dict(terms=terms, term_page_total=term_page_total)

    def get_sense_thesaurus_recursive(self):
        # Noudetaan koko merkitystesaurus käyttäen rekursiivista SQL-kyselyä
        # Etsitään ensin kaikki juurimerkityksien tietokantatunnisteet
        roots = self.session.query(Sense).filter(Sense.parent_id == None).all()
        root_list = []
        for root in roots:
            children = self.get_all_sense_children_recursive(root.identifier)
            root_list.append(dict(sense=dict(identifier=root.identifier, sense_text=str(root.sense),
                                             description=str(root.description)), children=children))

        return root_list

    def get_all_sense_children_recursive(self, parent_id):
        # Rekursiivinen SQL-kysely merkityksen lapsien hakemiseen
        parent_query = self.session.query(Sense)
        parent_query = parent_query.filter(Sense.parent_id == parent_id)
        parent_query = parent_query.cte('cte', recursive=True)
        child_query = self.session.query(Sense)
        child_query = child_query.join(parent_query, Sense.parent_id == parent_query.c.id)
        recursive_query = parent_query.union(child_query)
        result = self.session.query(recursive_query).all()
        
        # laitetaan kaikki kohteet oman parent_id alle
        sorted_results = dict()
        for child in result:
            if str(child.parent_id) not in sorted_results:
                sorted_results[str(child.parent_id)] = [dict(identifier=child.id,
                                                                sense_text=str(child.sense),
                                                                description=str(child.description),
                                                                children=[])]
            else:
                sorted_results[str(child.parent_id)].append(dict(identifier=child.id,
                                                                sense_text=str(child.sense),
                                                                description=str(child.description),
                                                                children=[]))
        # siirretään lista lapsia vanhemman alle
        for parent_key, parent_value in sorted_results.items():
            for child in parent_value:
                if str(child['identifier']) in sorted_results:
                    child['children'] = sorted_results[str(child['identifier'])]

        if str(parent_id) in sorted_results:
            return sorted_results[str(parent_id)]
        return []

    ## Termi ##

    def get_term_id(self, identifier):
        # Haetaan yksi termi
        term = self.session.query(Term).filter(Term.identifier == identifier).one_or_none()
        if not term:
            raise tornado.web.HTTPError(404)
        return term

    def get_term_senses(self, identifier, height, page):
        # Haetaan termiin liittyvät merkitykset
        senses = []
        term = self.get_term_id(identifier)
        sense_page_total = math.ceil(len(term.sense_terms)/height)
        single_sense_page = term.sense_terms[height*page:height*(page+1)]
        for sense_term in single_sense_page:
            senses.append(dict(identifier=sense_term.sense_id,
                               sense_text=str(self.get_sense_id(sense_term.sense_id).sense)))
        return dict(senses=senses, sense_page_total=sense_page_total)
    
    def get_term_variants(self, identifier, height, page):
        # Haetaan termiin liittyvät variantit
        variants = []
        term = self.get_term_id(identifier)
        for sense_term in term.sense_terms:
            for variant_usage in sense_term.variant_usages:
                variant = self.session.query(Variant)\
                                      .filter(Variant.identifier == variant_usage.variant_id)\
                                      .one()
                variants.append(dict(identifier=variant.identifier, variant_text=variant.name))
                
        # Järjestetään variantit aakkosjärjestykseen
        return sorted(variants, key=lambda k: k['variant_text']) 
    
    def get_term_family(self, identifier, height, depth, width, parent_page, child_page):
        # Haetaan termi ja 
          # height verran sen vanhempia
          # depth syvyydeltä lapsia
          # width verran lapsia per syvyyden taso
        main_term = self.get_term_id(identifier)
        parents = []
        children = []
        counter = 0
        parent = main_term.parent
        while counter < height and parent is not None:
            parents.append(dict(identifier=parent.identifier, term_text=parent.term,
                                etymology=parent.etymology, additional=parent.additional))
            parent = parent.parent
            counter += 1
        parents = reversed(parents)
        parent_page_total = math.ceil(counter / height)
        term_page_total = math.ceil(len(main_term.children) / width)
        children = self.get_term_child_list(main_term, depth, width, child_page)
        return dict(parents=parents, children=children['children'],
                    term=dict(identifier=main_term.identifier, term_text=str(main_term.term),
                              etymology=str(main_term.etymology), additional=str(main_term.additional)),
                    parent_page_total=parent_page_total, term_page_total=term_page_total)

    def get_term_child_list(self, parent, depth, width, page):
        # Syötteenä lapsi elementti, palauttaa listan lapsistaan ja niille listat niiden lapsista määriteltyyn syvyyteen
        depth -= 1
        child_list = []
        if depth >= 0:
            # Järjestetään lista vanhemman lapsista, ja haetaan halutulle sivunumerolle kuuluvat lapset
            children = sorted(parent.children, key=lambda x: x.term)[width*page:width*(page+1)]
            for child in children:
                child_list.append(self.get_term_child_list(child, depth, width, page))
        return dict(identifier=parent.identifier, term_text=str(parent.term), etymology=str(parent.etymology),
                    additional=parent.additional, children=child_list)

    def get_all_term_children(self, parent):
        child_list = []
        sense_list = []
        variant_list = []
        for sense_term in parent.sense_terms:
            # sense_term taulun kautta haetaan suoraan sense
            sense = sense_term.sense
            sense_list.append(dict(identifier=sense.identifier, sense_text=sense.sense, description=sense.description))
            for variant_usage in sense_term.variant_usages:
                # sense_term taulun kautta haetaan suoraan variant
                variant = variant_usage.variant
                variant_list.append(dict(identifier=variant.identifier, name=variant.name))
        for child in parent.children:
            child_list.append(self.get_all_term_children(child))
        return dict(identifier=parent.identifier, term_text=str(parent.term), etymology=str(parent.etymology),
                    additional=str(parent.additional), children=child_list, sense_list=sense_list,
                    variant_list=variant_list)


    def get_all_term_children_recursive(self, parent_id):
        # Rekursiivinen SQL-kysely määritellyn termin tai kaikkien juuritermien lapsien noutamiseen
        parent_query = self.session.query(Term.identifier,
                                          Term.parent_id,
                                          Term.term,
                                          Term.etymology,
                                          Term.additional,
                                          SenseTerm.identifier.label('st_id'),
                                          VariantUsage.identifier.label('vu_id'),
                                          Variant.identifier.label('v_id'),
                                          Variant.name,
                                          Sense.identifier.label('s_id'),
                                          Sense.sense,
                                          Sense.description)
        parent_query = parent_query.join(Term.sense_terms, isouter=True).join(SenseTerm.sense, isouter=True)\
                                    .join(SenseTerm.variant_usages, isouter=True).join(VariantUsage.variant, isouter=True)
        parent_query = parent_query.filter(Term.parent_id == parent_id).order_by(Term.identifier)
        parent_query = parent_query.cte('cte', recursive=True)
        

        child_query = self.session.query(Term.identifier,
                                          Term.parent_id,
                                          Term.term,
                                          Term.etymology,
                                          Term.additional,
                                          SenseTerm.identifier.label('st_id'),
                                          VariantUsage.identifier.label('vu_id'),
                                          Variant.identifier.label('v_id'),
                                          Variant.name,
                                          Sense.identifier.label('s_id'),
                                          Sense.sense,
                                          Sense.description)
        child_query = child_query.order_by(Term.identifier)
        child_query = child_query.join(Term.sense_terms).join(SenseTerm.sense).join(SenseTerm.variant_usages).join(VariantUsage.variant)
        child_query = child_query.join(parent_query, Term.parent_id == parent_query.c.id)

        recursive_query = parent_query.union(child_query)
        result = self.session.query(recursive_query).all()
        filtered_results = []
        seen_children = []
        related_data = dict()
        used_senses = []
        used_variants = []
        current_id = None
        # Kerätään lapsien merkitykset ja variantit, sitten poistetaan ylimääräiset rivit
        for child in result:
        
            if child.id != current_id:
                used_senses = []
                used_variants = []
                current_id = child.id
            
            if str(child.id) not in related_data:
                related_data[str(child.id)] = dict(senses=[], variants=[])
        
            if child.s_id is not None and child.s_id not in used_senses:
                used_senses.append(child.s_id)
                related_data[str(child.id)]['senses'].append(dict(identifier=child.s_id, sense_text=child.sense,
                                                                  description=child.description))
            if child.v_id is not None and child.v_id not in used_variants:
                used_variants.append(child.v_id)
                related_data[str(child.id)]['variants'].append(dict(identifier=child.v_id, name=child.name))

            if child.id not in seen_children:
                seen_children.append(child.id)
                filtered_results.append(child)

        # laitetaan kaikki kohteet oman parent_id alle
        sorted_results = dict()
        for child in filtered_results:
            if str(child.parent_id) not in sorted_results:
                sorted_results[str(child.parent_id)] = [dict(identifier=child.id,
                                                            term_text=str(child.term),
                                                            etymology=str(child.etymology),
                                                            additional=str(child.additional),
                                                            children=[],
                                                            sense_list=related_data[str(child.id)]['senses'],
                                                            variant_list=related_data[str(child.id)]['variants'])]
            else:
                sorted_results[str(child.parent_id)].append(dict(identifier=child.id,
                                                                term_text=str(child.term),
                                                                etymology=str(child.etymology),
                                                                additional=str(child.additional),
                                                                children=[],
                                                                sense_list=related_data[str(child.id)]['senses'],
                                                                variant_list=related_data[str(child.id)]['variants']))

        # siirretään lista lapsia vanhemman alle
        for parent_key, parent_value in sorted_results.items():
            for child in parent_value:
                if str(child['identifier']) in sorted_results:
                    child['children'] = sorted_results[str(child['identifier'])]
        
        if str(parent_id) in sorted_results:
            return sorted_results[str(parent_id)]
        return []

    def get_term_thesaurus(self, parent_id):
        # Noudetaan termitesaurus käyttäen
        # Etsitään ensin kaikki juuritermit tai määritellyn termin lapset
        roots = self.session.query(Term).filter(Term.parent_id == parent_id).all()
        root_list = []

        for root in roots:
            root_and_children = self.get_all_term_children(root)
            root_list.append(root_and_children)
        return root_list

    def get_term_thesaurus_recursive(self, parent_id):
        # Noudetaan koko termitesaurus käyttäen rekursiivista SQL-kyselyä
        return self.get_all_term_children_recursive(parent_id)


from setuptools import setup, find_packages

setup(
    name='sanakirja',
    version='0.1.0',
    url='',
    description=('Hierarkisien näkymien generointi relaatiotietokannan sanakirjadatasta.'),
    author='Jukka',
    packages=find_packages(),
    install_requires=[
        'psycopg2-binary',
        'sqlalchemy',
        'configargparse',
        'tornado',
        'lxml',
    ]
)

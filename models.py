# coding: utf-8
from sqlalchemy import Boolean, CHAR, CheckConstraint, Column, DateTime, ForeignKey, Integer, String, Table, Text, text
from sqlalchemy.orm import relationship
from sqlalchemy.dialects.postgresql import TIMESTAMP
from sqlalchemy.ext.declarative import declarative_base

Base = declarative_base()
metadata = Base.metadata


class BookReference(Base):
    __tablename__ = 'book_references'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('book_references_id_seq'::regclass)"))
    parent_id = Column(ForeignKey('book_references.id'))
    text = Column(String(100), nullable=False)
    title = Column(Text, nullable=False)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)

    parent = relationship('BookReference', remote_side="BookReference.identifier")


t_category_group_list = Table(
    'category_group_list', metadata,
    Column('categorygroupid', String, nullable=False),
    Column('categorygroupabbr', String(40), nullable=False),
    Column('categorygroupname', String(65), nullable=False)
)


class CategoryGroup(Base):
    __tablename__ = 'category_groups'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('category_groups_id_seq'::regclass)"))
    abbreviation = Column(String(40), nullable=False)
    name = Column(String(65), nullable=False)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)


t_category_list = Table(
    'category_list', metadata,
    Column('categoryid', String(2), nullable=False),
    Column('categorygroupid', String),
    Column('categoryabbr', String(40), nullable=False),
    Column('categoryname', String(65), nullable=False)
)


t_history = Table(
    'history', metadata,
    Column('termid', Integer, nullable=False),
    Column('etymology', String(255)),
    Column('additional', Text),
    Column('attn', Integer)
)


t_idset = Table(
    'idset', metadata,
    Column('id', Integer)
)


t_language_list = Table(
    'language_list', metadata,
    Column('languageid', String(3), nullable=False),
    Column('languagename', String(15), nullable=False)
)


class Language(Base):
    __tablename__ = 'languages'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('languages_id_seq'::regclass)"))
    name = Column(String(3), nullable=False, unique=True)
    description = Column(String(15), nullable=False)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)


t_lexical_field_list = Table(
    'lexical_field_list', metadata,
    Column('lexicalfieldid', String(3), nullable=False),
    Column('lexicalfieldname', String(15), nullable=False)
)


class LexicalField(Base):
    __tablename__ = 'lexical_fields'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('lexical_fields_id_seq'::regclass)"))
    name = Column(CHAR(3), nullable=False, unique=True)
    description = Column(String(15), nullable=False)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)


t_number_list = Table(
    'number_list', metadata,
    Column('numberid', String(5), nullable=False),
    Column('numbername', String(50), nullable=False)
)


class Number(Base):
    __tablename__ = 'numbers'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('numbers_id_seq'::regclass)"))
    name = Column(String(5), nullable=False, unique=True)
    description = Column(String(50), nullable=False)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)


class PageType(Base):
    __tablename__ = 'page_types'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('page_types_id_seq'::regclass)"))
    name = Column(String(2), nullable=False, unique=True)
    description = Column(String(30), nullable=False)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)


t_pagetype_list = Table(
    'pagetype_list', metadata,
    Column('pagetype', String(2), nullable=False),
    Column('pagetypename', String(10), nullable=False)
)


t_publishing = Table(
    'publishing', metadata,
    Column('variantid', Integer, nullable=False),
    Column('publishinwww', Boolean),
    Column('publishinprint', Boolean),
    Column('timestamp', TIMESTAMP(precision=2))
)


t_purpose_list = Table(
    'purpose_list', metadata,
    Column('purposeid', String(4), nullable=False),
    Column('purposename', String(65), nullable=False)
)


t_quote_list = Table(
    'quote_list', metadata,
    Column('quoteid', Integer, nullable=False),
    Column('textid', String(10)),
    Column('folio', String(5), nullable=False),
    Column('pagetype', String(2)),
    Column('line', Integer, nullable=False),
    Column('quote', Text),
    Column('purposeid', String(4))
)


t_quoteid_seed = Table(
    'quoteid_seed', metadata,
    Column('last_quoteid', Integer)
)


t_reference_list = Table(
    'reference_list', metadata,
    Column('refid', Integer, nullable=False),
    Column('parentrefid', Integer, nullable=False, server_default=text("(-1)")),
    Column('referencetext', String(100), nullable=False),
    Column('referencetitle', Text)
)


t_refid_seed = Table(
    'refid_seed', metadata,
    Column('last_refid', Integer)
)


t_selectnode = Table(
    'selectnode', metadata,
    Column('term', String),
    Column('termid', Integer),
    Column('generation', Integer)
)


t_senseid_seed = Table(
    'senseid_seed', metadata,
    Column('last_senseid', Integer)
)


t_senseorder = Table(
    'senseorder', metadata,
    Column('sequencenumber', Integer, nullable=False),
    Column('termsenseid', Integer, nullable=False),
    Column('timestamp', TIMESTAMP(precision=2))
)


class Sense(Base):
    __tablename__ = 'senses'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('senses_id_seq'::regclass)"))
    sense = Column(String(255), nullable=False)
    description = Column(Text, nullable=False)
    has_children = Column(Boolean, nullable=False, server_default=text("false"))
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)

    # Vanhemman ID ja relaatio siihen
    parent_id = Column(ForeignKey('senses.id'))
    parent = relationship('Sense', remote_side="Sense.identifier")
    # Manuaalisesti lisätty relaatio lapsi elementtien helpolle noutamiselle
    children = relationship('Sense')
    # Manuualisesti lisätty relaatio sense_termssien helpolle noutamiselle
    sense_terms = relationship("SenseTerm")


class Tag(Base):
    __tablename__ = 'tags'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('tags_id_seq'::regclass)"))
    name = Column(String(15), nullable=False)
    description = Column(String(255))
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)


class TermSource(Base):
    __tablename__ = 'term_sources'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('term_sources_id_seq'::regclass)"))
    name = Column(String(6), nullable=False, unique=True)
    description = Column(String(30), nullable=False)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)


t_termid_seed = Table(
    'termid_seed', metadata,
    Column('last_termid', Integer)
)


t_termsense_pairs = Table(
    'termsense_pairs', metadata,
    Column('termsenseid', Integer, nullable=False),
    Column('termid', Integer),
    Column('senseid', Integer),
    Column('numberid', String(5)),
    Column('med', Integer),
    Column('oed', Integer)
)


t_termsenseid_seed = Table(
    'termsenseid_seed', metadata,
    Column('last_termsenseid', Integer)
)


t_termsource_list = Table(
    'termsource_list', metadata,
    Column('termsourceid', String(6), nullable=False),
    Column('termsourcename', String(30))
)


t_text_list = Table(
    'text_list', metadata,
    Column('textid', String(10), nullable=False),
    Column('textsignum', String(25), nullable=False),
    Column('texttypeid', String(2)),
    Column('texttitle', Text, nullable=False),
    Column('textyear', String(20), nullable=False),
    Column('manusyear', String(4)),
    Column('myprecision', String(3)),
    Column('urtextyear', String(4)),
    Column('utyprecision', String(3))
)


t_text_list_ordered = Table(
    'text_list_ordered', metadata,
    Column('textid', String(10)),
    Column('textsignum', String(25)),
    Column('texttypeid', String(2)),
    Column('texttitle', Text),
    Column('textyear', String(20)),
    Column('manusyear', String(4)),
    Column('myprecision', String(3)),
    Column('urtextyear', String(4)),
    Column('utyprecision', String(3)),
    Column('base_year', Text),
    Column('year_prefix', Text),
    Column('fuzzy_year', Text)
)


class TextType(Base):
    __tablename__ = 'text_types'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('text_types_id_seq'::regclass)"))
    name = Column(String(2), nullable=False, unique=True)
    description = Column(String(30), nullable=False)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)


t_texttype_list = Table(
    'texttype_list', metadata,
    Column('texttypeid', String(2), nullable=False),
    Column('texttype', String(30), nullable=False)
)


t_treeterm = Table(
    'treeterm', metadata,
    Column('ordinal', Integer, nullable=False),
    Column('termid', Integer, nullable=False),
    Column('term', String, nullable=False),
    Column('generation', Integer, nullable=False),
    Column('variants', Integer, nullable=False),
    Column('senses', Integer, nullable=False),
    Column('parentid', Integer, nullable=False),
    Column('descendanthasvariants', Boolean, nullable=False),
    Column('descendanthassenses', Boolean, nullable=False)
)


t_userrole = Table(
    'userrole', metadata,
    Column('userid', Integer, nullable=False),
    Column('password', String(40)),
    Column('username', String(10)),
    Column('role', CHAR(1))
)


t_variant_status = Table(
    'variant_status', metadata,
    Column('variantid', Integer, nullable=False),
    Column('xref', Boolean),
    Column('show', Boolean),
    Column('plurality', String(4)),
    Column('timestamp', TIMESTAMP(precision=2)),
    CheckConstraint("((plurality)::text = 'pl'::text) OR ((plurality)::text = 'sgpl'::text)")
)


class Variant(Base):
    __tablename__ = 'variants'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('variants_id_seq'::regclass)"))
    name = Column(String(50), nullable=False, unique=True)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)


t_year_precision_list = Table(
    'year_precision_list', metadata,
    Column('yearprecision', String(3), nullable=False)
)


class YearPrecision(Base):
    __tablename__ = 'year_precisions'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('year_precisions_id_seq'::regclass)"))
    name = Column(String(3), nullable=False, unique=True)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)


class Category(Base):
    __tablename__ = 'categories'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('categories_id_seq'::regclass)"))
    abbreviation = Column(String(40), nullable=False)
    name = Column(String(65), nullable=False)
    category_group_id = Column(ForeignKey('category_groups.id'), nullable=False)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)

    category_group = relationship('CategoryGroup')


class Term(Base):
    __tablename__ = 'terms'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('terms_id_seq'::regclass)"))
    term = Column(String(50), nullable=False, unique=True)
    parent_id = Column(ForeignKey('terms.id'))
    left_idx = Column(Integer, index=True)
    right_idx = Column(Integer, index=True)
    term_source_id = Column(ForeignKey('term_sources.id'), nullable=False)
    doe_sv = Column(String(50))
    med_sv = Column(String(50))
    oed_sv = Column(String(50))
    med_manus_year = Column(String(4))
    med_myprecision = Column(CHAR(3))
    med_year_ref = Column(String(25))
    oed_manus_year = Column(String(4))
    oed_myprecision = Column(CHAR(3))
    oed_year_ref = Column(String(25))
    etymology = Column(String(255))
    additional = Column(Text)
    attention = Column(Boolean, nullable=False, server_default=text("false"))
    attention_details = Column(Text)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)

    parent = relationship('Term', remote_side="Term.identifier")
    term_source = relationship('TermSource')
    # Manuaalisesti lisätty relaatio lapsi elementtien helpolle noutamiselle
    children = relationship('Term')
    # Manuualisesti lisätty relaatio sense_termssien helpolle noutamiselle
    sense_terms = relationship("SenseTerm")

class Texts(Base):
    __tablename__ = 'texts'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('texts_id_seq'::regclass)"))
    call_number = Column(String(10), nullable=False, unique=True)
    signum = Column(String(25), nullable=False)
    text_type_id = Column(ForeignKey('text_types.id'), nullable=False)
    title = Column(Text, nullable=False)
    text_year = Column(String(20), nullable=False)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)

    text_type = relationship('TextType')


class CategoriesTerm(Base):
    __tablename__ = 'categories_terms'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('categories_terms_id_seq'::regclass)"))
    category_id = Column(ForeignKey('categories.id'), nullable=False)
    term_id = Column(ForeignKey('terms.id'), nullable=False, index=True)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)

    category = relationship('Category')
    term = relationship('Term')


class LexicalFieldsTerm(Base):
    __tablename__ = 'lexical_fields_terms'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('lexical_fields_terms_id_seq'::regclass)"))
    lexical_field_id = Column(ForeignKey('lexical_fields.id'), nullable=False)
    term_id = Column(ForeignKey('terms.id'), nullable=False, index=True)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)

    lexical_field = relationship('LexicalField')
    term = relationship('Term')


class Quote(Base):
    __tablename__ = 'quotes'
    __table_args__ = (
        CheckConstraint("((purpose)::text = 'DICT'::text) OR ((purpose)::text = 'OTHER'::text)"),
    )

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('quotes_id_seq'::regclass)"))
    folio = Column(String(5), nullable=False)
    line = Column(Integer)
    quote = Column(Text, nullable=False)
    text_id = Column(ForeignKey('texts.id'), nullable=False)
    page_type_id = Column(ForeignKey('page_types.id'), nullable=False)
    purpose = Column(String(5), nullable=False, server_default=text("'DICT'::character varying"))
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)

    page_type = relationship('PageType')
    text = relationship('Texts')


class ReferencesTerm(Base):
    __tablename__ = 'references_terms'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('references_terms_id_seq'::regclass)"))
    term_id = Column(ForeignKey('terms.id'), nullable=False)
    reference_id = Column(ForeignKey('book_references.id'), nullable=False)
    page = Column(String(40))
    comment = Column(String(255))
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)

    reference = relationship('BookReference')
    term = relationship('Term')


class SenseTerm(Base):
    __tablename__ = 'sense_terms'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('sense_terms_id_seq'::regclass)"))
    sense_id = Column(ForeignKey('senses.id'), nullable=False)
    term_id = Column(ForeignKey('terms.id'))
    med = Column(Integer)
    oed = Column(Integer)
    number_id = Column(ForeignKey('numbers.id'))
    sequence_number = Column(Integer, nullable=False, server_default=text("1"))
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)

    number = relationship('Number')
    sense = relationship('Sense')
    term = relationship('Term')
    #Manuaalisesti lisätty
    variant_usages = relationship("VariantUsage")

class TagsTerm(Base):
    __tablename__ = 'tags_terms'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('tags_terms_id_seq'::regclass)"))
    tag_id = Column(ForeignKey('tags.id'), nullable=False)
    term_id = Column(ForeignKey('terms.id'), nullable=False)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)

    tag = relationship('Tag')
    term = relationship('Term')


class QuoteVariant(Base):
    __tablename__ = 'quote_variants'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('quote_variants_id_seq'::regclass)"))
    variant_id = Column(ForeignKey('variants.id'), nullable=False)
    phrase = Column(String(100))
    line = Column(Integer)
    in_marg = Column(Boolean, server_default=text("false"))
    quote_id = Column(ForeignKey('quotes.id'), nullable=False)
    publish_in_www = Column(Boolean)
    publish_in_print = Column(Boolean)
    page_type_id = Column(ForeignKey('page_types.id'), nullable=False)
    sense_term_id = Column(ForeignKey('sense_terms.id'))
    language_id = Column(ForeignKey('languages.id'), nullable=False)
    folio = Column(String(5), nullable=False)
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)

    language = relationship('Language')
    page_type = relationship('PageType')
    quote = relationship('Quote')
    sense_term = relationship('SenseTerm')
    variant = relationship('Variant')


class VariantUsage(Base):
    __tablename__ = 'variant_usages'

    identifier = Column('id', Integer, primary_key=True, server_default=text("nextval('variant_usages_id_seq'::regclass)"))
    variant_id = Column(ForeignKey('variants.id'), nullable=False)
    xref = Column(Boolean)
    show = Column(Boolean)
    plurality = Column(String(4))
    sense_term_id = Column(ForeignKey('sense_terms.id'))
    created = Column(DateTime, nullable=False)
    modified = Column(DateTime, nullable=False)

    sense_term = relationship('SenseTerm')
    variant = relationship('Variant')

import tornado.ioloop
import tornado.web
import configargparse
import db_handler
import json
import logging
from lxml import (
    etree,
    objectify
)

SENSE_PATH = '/html/sense/'
TERM_PATH = '/html/term/'
db_session = None

## Merkitys XML generointi

def sense_maker(identifier, text, description):
    # yksittäisen merkityselementin luonti
    sense = etree.Element('sense', identifier='sense' + str(identifier))
    sense_text = etree.SubElement(sense, 'senseText')
    sense_text.text = text
    description_elem = etree.SubElement(sense, 'description')
    description_elem.text = description
    return sense

def children_sense_xml(children):
    # muodostetaan rekursiivisesti lapsielementit ja niiden lapsielementit
    xml_elements = []
    for child in children:
        sense = sense_maker(child['identifier'], child['sense_text'], child['description'])
        inner_children = children_sense_xml(child['children'])
        for inner_child in inner_children:
            narrower = etree.Element('narrower')
            narrower.append(inner_child)
            sense.append(narrower)
        xml_elements.append(sense)
    return xml_elements


def create_sense_xml(senses_dict):
    thesaurus = etree.Element('thesaurus')
    sense = sense_maker(senses_dict['sense']['identifier'], senses_dict['sense']['sense_text'],
                        senses_dict['sense']['description'])
    thesaurus.append(sense)
    broad_sense = sense
    for parent in senses_dict['parents']:
        broader = etree.SubElement(broad_sense, 'broader')
        broad_sense = sense_maker(parent['identifier'], parent['sense_text'], parent['description'])
        broader.append(broad_sense)

    children = children_sense_xml(senses_dict['children'])
    for child in children:
        narrower = etree.Element('narrower')
        narrower.append(child)
        sense.append(narrower)
    return etree.tostring(thesaurus, pretty_print=True)


def create_sense_thesaurus_xml(senses_list):
    # Luodaan juurielementti ja lisätään sen alle tietokannasta noudettu data
    thesaurus = etree.Element('thesaurus')
    for senses_dict in senses_list:
        # Muodostetaan merkityksen elementti
        sense = sense_maker(senses_dict['sense']['identifier'], senses_dict['sense']['sense_text'],
                            senses_dict['sense']['description'])
        thesaurus.append(sense)
        # Muodostetaan merkityksen lapsielementit
        children = children_sense_xml(senses_dict['children'])
        for child in children:
            narrower = etree.Element('narrower')
            narrower.append(child)
            sense.append(narrower)
    return etree.tostring(thesaurus, pretty_print=True)


## Termi XML generointi

def term_maker(term_dict):
    # yksittäisen termielementin luonti
    term = etree.Element('term', identifier='term' + str(term_dict['identifier']))
    term_text_elem = etree.SubElement(term, 'termText')
    term_text_elem.text = term_dict['term_text']
    etymology_elem = etree.SubElement(term, 'etymology')
    etymology_elem.text = term_dict['etymology']
    additional_elem = etree.SubElement(term, 'additional')
    additional_elem.text = term_dict['additional']
    if 'sense_list' in term_dict:
        senses = etree.Element('senses')
        for sense in term_dict['sense_list']:
            senses.append(sense_maker(sense['identifier'], sense['sense_text'], sense['description']))
        term.append(senses)
    if 'variant_list' in term_dict:
        variants = etree.Element('variants')
        for variant in term_dict['variant_list']:
            variant_elem = etree.Element('variant', identifier='variant' + str(variant['identifier']))
            variant_text = etree.SubElement(variant_elem, 'variantText')
            variant_text.text = variant['name']
            variants.append(variant_elem)
        term.append(variants)
    children = children_term_xml(term_dict['children'])
    narrower_terms = etree.Element('narrowerTerms')
    for child in children:
        narrower_terms.append(child)
    term.append(narrower_terms)
    return term


def children_term_xml(children):
    # muodostetaan rekursiivisesti lapsielementit ja niiden lapsielementit
    xml_elements = []
    for child in children:
        if 'sense_list' in child:
            sense_list = child['sense_list']
        else:
            sense_list = None
        term = term_maker(child)
        inner_children = children_term_xml(child['children'])

        xml_elements.append(term)
    return xml_elements


def create_term_xml(terms_dict):
    thesaurus = etree.Element('thesaurus')
    term = term_maker(terms_dict)
    thesaurus.append(term)
    # vanhemmat
    broad_term = term
    for parent in terms_dict['parents']:
        broader = etree.SubElement(broad_term, 'broader')
        broad_term = term_maker(parent)
        broader.append(broad_term)

    return etree.tostring(thesaurus, pretty_print=True)


def create_term_thesaurus_xml(terms_list):
    # Luodaan juurielementti ja lisätään sen alle tietokannasta noudettu data
    thesaurus = etree.Element('thesaurus')
    for terms_dict in terms_list:
        term = term_maker(terms_dict)
        thesaurus.append(term)

    return etree.tostring(thesaurus, pretty_print=True)


## Merkitys requesthandlerit ##

class SenseXmlHandler(tornado.web.RequestHandler):
    # Haetaan määritellylle juurimerkitykselle n syvyydellä tesaurus xml muodossa
    def get(self, identifier):
        height = self.get_argument('height', 4)
        depth = self.get_argument('depth', 4)
        with db_session.session_manager():
            senses = db_session.get_sense_family(identifier, height, depth)
            xml_str = create_sense_xml(senses)
            self.set_header('Content-Type', 'application/xml')
            self.write(xml_str)

class SenseThesaurusHandler(tornado.web.RequestHandler):
    # haetaan koko merkitystesaurus
    def get(self):
        with db_session.session_manager():
            thesaurus_data = db_session.get_sense_thesaurus()
            xml_str = create_sense_thesaurus_xml(thesaurus_data)
            self.set_header('Content-Type', 'application/xml')
            self.write(xml_str)

class SenseThesaurusRecursiveHandler(tornado.web.RequestHandler):
    # Haetaan koko merkitystesaurus rekursiivisella SQL-kyselyllä
    def get(self):
        with db_session.session_manager():
            thesaurus_data = db_session.get_sense_thesaurus_recursive()
            xml_str = create_sense_thesaurus_xml(thesaurus_data)
            self.set_header('Content-Type', 'application/xml')
            self.write(xml_str)

## Termi requesthandlerit ##

class TermXmlHandler(tornado.web.RequestHandler):
    # Haetaan määritellylle juuritermille n syvyydellä tesaurus xml muodossa
    def get(self, identifier):
        height = self.get_argument('height', 4)
        depth = self.get_argument('depth', 4)
        with db_session.session_manager():
            terms = db_session.get_term_family(identifier, height, depth)
            xml_str = create_term_xml(terms)
            self.set_header('Content-Type', 'application/xml')
            self.write(xml_str)

class TermThesaurusHandler(tornado.web.RequestHandler):
    # Haetaan koko termitesaurus
    # Sallii myös juuren määrittelyn
    def get(self, parent_id=None):
        with db_session.session_manager():
            thesaurus_data = db_session.get_term_thesaurus(parent_id)
            xml_str = create_term_thesaurus_xml(thesaurus_data)
            self.set_header('Content-Type', 'application/xml')
            self.write(xml_str)


class TermThesaurusRecursiveHandler(tornado.web.RequestHandler):
    # Haetaan koko termitesaurus rekursiivisella SQL-kyselyllä
    # Sallii myös juuren määrittelyn
    def get(self, parent_id=None):
        with db_session.session_manager():

            thesaurus_data = db_session.get_term_thesaurus_recursive(parent_id)
            xml_str = create_term_thesaurus_xml(thesaurus_data)
            self.set_header('Content-Type', 'application/xml')
            self.write(xml_str)
            
## Käyttöliittymän requesthandlerit ##

class HtmlHandler(tornado.web.RequestHandler):
    def initialize(self, height, depth, width):
        self.height = height
        self.depth = depth
        self.width = width

    def prepare(self):
        try:
            self.height = int(self.get_argument('height', self.height))
        except ValueError:
            logging.warning('Argument height must be integer')
        try:
            self.depth = int(self.get_argument('depth', self.depth))
        except ValueError:
            logging.warning('Argument depth must be integer')
        try:
            self.width = int(self.get_argument('width', self.width))
        except ValueError:
            logging.warning('Argument width must be integer')
        try:
            self.parent_page = int(self.get_argument('parent_page', 1))
        except ValueError:
            logging.warning('Argument parent_page must be integer')
            self.parent_page = 0
        try:
            self.sense_page = int(self.get_argument('sense_page', 1))
        except ValueError:
            logging.warning('Argument sense_page must be integer')
            self.sense_page = 0
        try:
            self.term_page = int(self.get_argument('term_page', 1))
        except ValueError:
            logging.warning('Argument term_page must be integer')
            self.sense_page = 0

class SenseHtmlHandler(HtmlHandler):
    def get(self, identifier):
        with db_session.session_manager():
            # Noudetaan käyttöliittymän tarvisema data
            senses = db_session.get_sense_family(identifier, self.height, self.depth, self.width, self.parent_page - 1,
                                                 self.sense_page - 1)
            terms = db_session.get_sense_terms(identifier, self.height, self.term_page - 1)

        # Renderöidään datalle käyttöliittymä käyttäen pohjaa
        self.render("sense_template.html", sense_path=SENSE_PATH, term_path=TERM_PATH,
                    sense=senses['sense'], parents=senses['parents'], senses=senses['children'], terms=terms['terms'],
                    parent_page=self.parent_page, sense_page=self.sense_page, term_page=self.term_page,
                    parent_page_total=senses['parent_page_total'], sense_page_total=senses['sense_page_total'],
                    term_page_total=terms['term_page_total'])

class TermHtmlHandler(HtmlHandler):
    def get(self, identifier):
        with db_session.session_manager():
            # Noudetaan käyttöliittymän tarvisema data
            terms = db_session.get_term_family(identifier, self.height, self.depth, self.width, self.parent_page - 1,
                                               self.term_page - 1)
            senses = db_session.get_term_senses(identifier, self.height, self.sense_page - 1)

            variants = db_session.get_term_variants(identifier, 5, 0)
            variants_string = ""
            for variant in variants:
                variants_string += variant['variant_text'] + ", "
            variants_string = variants_string[:-2]
            
        # Renderöidään datalle käyttöliittymä käyttäen pohjaa
        self.render("term_template.html", sense_path=SENSE_PATH, term_path=TERM_PATH,
                    term=terms['term'], parents=terms['parents'], terms=terms['children'], senses=senses['senses'],
                    parent_page=self.parent_page, sense_page=self.sense_page, term_page=self.term_page,
                    parent_page_total=terms['parent_page_total'], sense_page_total=senses['sense_page_total'],
                    term_page_total=terms['term_page_total'],
                    variants=variants_string)


def make_app(args):
    return tornado.web.Application([(r"/static/(.*)", tornado.web.StaticFileHandler, {"path": "static"}),
                                    (r"/xml/sense/(?P<identifier>[0-9]+)", SenseXmlHandler),
                                    (r"/xml/sense/thesaurus", SenseThesaurusHandler),
                                    (r"/xml/sense/thesaurus_recursive", SenseThesaurusRecursiveHandler),
                                    (r"/html/sense/(?P<identifier>[0-9]+)", SenseHtmlHandler,
                                     dict(height=args.height, depth=args.depth, width=args.width)),

                                    (r"/xml/term/(?P<identifier>[0-9]+)", TermXmlHandler),
                                    (r"/xml/term/thesaurus", TermThesaurusHandler),
                                    (r"/xml/term/thesaurus_recursive", TermThesaurusRecursiveHandler),
                                    (r"/html/term/(?P<identifier>[0-9]+)", TermHtmlHandler,
                                     dict(height=args.height, depth=args.depth, width=args.width)),
                                    (r"/xml/term/thesaurus/(?P<parent_id>[0-9]+)", TermThesaurusHandler),
                                    (r"/xml/term/thesaurus_recursive/(?P<parent_id>[0-9]+)", TermThesaurusRecursiveHandler)])


if __name__ == "__main__":
    parser = configargparse.ArgParser()
    parser.add('-c', '--custom_config', required=True, is_config_file=True)
    parser.add('--logging_level', default='WARNING',
               choices=['DEBUG', 'INFO', 'WARNING', 'ERROR', 'CRITICAL'], required=True)
    parser.add('--port', type=int, required=True)
    parser.add('--db_url', required=True)
    parser.add('--indent_logging', type=bool, default=False, required=True)
    parser.add('--height', type=int, default=25, required=True,
               help='The number of parents fetched for a term or sense')
    parser.add('--depth', type=int, default=3, required=True,
               help='The number of layers of children fetched for a term or sense')
    parser.add('--width', type=int, default=25, required=True,
               help='The number of ammounth of children per layer')
    args = parser.parse_args()
    logging.basicConfig(level=args.logging_level)
    db_session = db_handler.DBHandler(args.db_url)
    app = make_app(args)
    app.listen(args.port)
    logging.info('Tornado Thesaurus now running')
    tornado.ioloop.IOLoop.current().start()

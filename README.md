# Toteutus: Hierarkkisien näkymien generointi relaatiotietokannan sanakirjadatasta #
Vieruslistaesitystä käyttäen relaatiotietokantaan tallennetun hierarkkisen sanakirjadatan noutaminen rekursiivisella SQL-kyselyllä ja noudetun datan perusteella tesauruksen generointi XML- ja HTML-dokumenttien muodossa.
